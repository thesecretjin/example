var stdModel = require('../models/stdschema');

exports.insertStd = insertStd;

async function insertStd(model){
    var select = await stdModel.find({name: model.name})
    if(select.length === 0 ){
        const payload = model
        const result = await new stdModel(payload)
        await result.save()
        return({status:true,data:'insert data complete',obj:payload} )   
     }else{
        return({status:false,data:'already have data student',obj:null})
    }
}