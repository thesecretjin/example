var stdDb = require('../db/std.db');
exports.managedata=(app)=>{
    app.post('/insertStd',insertStd)
}
async function insertStd(req,res){
        try{
            var result = await stdDb.insertStd(req.body)
            .then(function(response){
                res.status(200).json(response);
                return;
            });
        }catch (e){
            console.log("\n\n _______________________ERROR_____________________\n" ,e)
            return({status:false,data:'internal server',obj:null}).status(200).end()        
        }
}