const express = require('express')
const app = express()
const port=3000
const mongoose = require('mongoose')
const ObjectId=  mongoose.Types.ObjectId

//const insertstd= require('./module/insertStd.module')
const insert = require('./module/insertStd.module')
const connectdb=require('./connectdb.db')


app.use(express.json())
connectdb.setup(app)
insert.managedata(app)
    
app.listen(port,()=> console.log(`example app listening on port ${port}!`))
