const mongoose = require('mongoose');
const url ='mongodb://localhost:27017/std'
const authSources = { 
	useNewUrlParser: true,
	authSource:'admin',
	useUnifiedTopology: true
} || { 
	useNewUrlParser: true
};
exports.setup = function (app) {
	mongoose.Promise = global.Promise;
	mongoose.connect(url,authSources).then(() => {
	    console.log("Successfully connected to the database project std");    
	}).catch(err => {
	    console.log('Could not connect to the database inteGreat. Exiting now...');
	    process.exit();
	});
}

